#include <iostream>
#include <cstdlib>

using namespace std;

void printTaskDescription() {
  cout << "Napište program, který vypíše všechna prvočísla ze zvoleného rozsahu. "
          "Na vstupu jsou dvě celá kladná čísla udávající začátek a konec intervalu (včetně). "
          "Na výstupu je seznam nalezených prvočísel" << endl;
}

bool readInterval(int* x0, int* x1) {
  cout << "Zadejte dolní mez intervalu (celé číslo):" << endl;
  cin >> *x0;
  if (*x0 <= 0) {
    cout << "Číslo musí být větší než nula";
    return false;
  }
  cout << "Zadejte horní mez intervalu (celé číslo):" << endl;
  cin >> *x1;
  if (*x1 <= 0) {
    cout << "Číslo musí být větší než nula";
    return false;
  }
  if (*x0 > *x1) {
    cout << "Spodní mez je větší než horní. Konec programu." << endl;
    return false;
  }
  return true;
}

bool isPrime(int x) {
    int count = 0;
    for (int i = 1; i <= x; i ++) {
      if (x % i == 0) {
        count ++;
      }
    }
    return count == 2;
}

int main (void) {

  printTaskDescription();

  int x0,x1;

  if (!readInterval(&x0, &x1)) {
      return EXIT_FAILURE;
  }

  cout << "Zadali jste: " << x0 << " a " << x1 << endl;

  cout << "Výsledek:" << endl;

  bool first = true;

  for (int i = x0; i <= x1; i++) {
    if (isPrime(i)) {
      if (!first) {
        cout << ", ";
      } else {
        first = false;
      }
      cout << i;
    }
  }

  return EXIT_SUCCESS;
}

