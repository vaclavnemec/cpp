#include <iostream>
#include <cstdlib>

using namespace std;

void printTaskDescription() {
  cout << "Úkolem je realizovat program, který rozhodne o existenci trojúhelníku."
          "Vstupem programu jsou tři desetinná čísla - délky stran trojúhelníku."
          "Výstupem programu je rozhodnutí, zda trojelník existuje";
}

bool readLengths(float* l1, float* l2, float* l3) {
  cout << "Zadejte délku první strany trojúhelníku:" << endl;
  cin >> *l1;
  if (*l1 <= 0) {
    cout << "Číslo musí být větší než nula";
    return false;
  }
  cout << "Zadejte délku druhé strany trojúhelníku:" << endl;
  cin >> *l2;
  if (*l2 <= 0) {
    cout << "Číslo musí být větší než nula";
    return false;
  }
  cout << "Zadejte délku třetí strany trojúhelníku:" << endl;
  cin >> *l3;
  if (*l3 <= 0) {
    cout << "Číslo musí být větší než nula";
    return false;
  }
  return true;
}

int main(void) {
  float a, b, c;

  if (!readLengths(&a, &b, &c)) {
      return EXIT_FAILURE;
  }

  if ((a + b <= c) or (a + c <= b) or (b + c <= a)) {
    cout << "Zadaným délkám neodpovídá žádný trojúhelník";
  } else {
    cout << "Zadané délky popisují trojúhelník";
  }

  return EXIT_SUCCESS;
}
