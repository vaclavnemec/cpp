#include <iostream>
#include <cstdlib>

using namespace std;

void printTaskDescription() {
  cout << "Naimplementujte funkce řešící tyto problémy a demonstrujte jejich funkčnost:" << endl
    << "1. Funkce vypisující binární reprezentaci celého neznaménkového čísla." << endl
    << "    (Dokážete vypsat všechny bity daného čísla? (sizeof))" << endl
    << "2. Užitím výše implementované funkce demonstrujte funkčnost binárních operací (AND &, OR |, XOR ^, NOT ~)." << endl
    << "3. Výpis počtu jedničkových bitù v zadaném celém čísle (pozor na záporná)." << endl
    << "4. Nejvyšší společný dělitel dvou celých čísel." << endl
    << "5. (*) Funkce, která rozhoduje podle data narození, která ze dvou osob je starší a u obou vypíše věk." << endl
    << "    (time_t)" << endl;
}

void printBinary(unsigned int number) {
  for(short i=sizeof(unsigned int) * 8 - 1; i >= 0; i--){
    if (i % 8 == 7) {
      cout << ' ';
    }
    cout << (((number)>>i)&1);
  }
  cout << endl;
}

int amoutOfHighBits(int a) {
  if (a < 0) {
    a = -a;
  }
  int amount = 0;
  for(short i=sizeof(unsigned int) * 8 - 1; i >= 0; i--){
    if (i % 8 == 7) {
      cout << ' ';
    }
    if(((a)>>i)&1) {
      amount ++;
    }
  }
  return amount;
}

int main (void) {
  printTaskDescription();

  unsigned int a = 0b10101010101010101010101010101010;
  unsigned int b = 0b01010101010101010101010101010101;

  cout << "a" << endl;
  printBinary(a);
  cout << "b" << endl;
  printBinary(b);
  cout << "a & b" << endl;
  printBinary(a & b);
  cout << "a | b" << endl;
  printBinary(a | b);
  cout << "a ^ b" << endl;
  printBinary(a ^ b);
  cout << "~a" << endl;
  printBinary(~a);

  cout << "Pocet jednicek v 0b10101010101010101010101010101010: " << amoutOfHighBits(0b10101010101010101010101010101010);

}
